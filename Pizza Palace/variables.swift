//
//  variables.swift
//  Pizza Palace
//
//  Created by David Keen on 11/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Foundation

//Setup global values

// [Int:Int]() = [key of type Int: value of type Int](),
//() = Initilise empty dictionary

var cheeseAndTomatoQuantity = 0
var cheeseAndTomatoExtrasChillie = [Int:Int]()
var cheeseAndTomatoExtrasPineapple = [Int:Int]()
var cheeseAndTomatoExtrasCheese = [Int:Int]()
var cheeseAndTomatoThinBases = 0

var meetFeastQuantity = 0
var meetFeastExtrasChillie = [Int:Int]()
var meetFeastExtrasPineapple = [Int:Int]()
var meetFeastExtrasCheese = [Int:Int]()
var meetFeastThinBases = 0

var pepperoniQuantity = 0
var pepperoniExtrasChillie = [Int:Int]()
var pepperoniExtrasPineapple = [Int:Int]()
var pepperoniExtrasCheese = [Int:Int]()
var pepperoniThinBases = 0

var vegFeastQuantity = 0
var vegFeastExtrasChillie = [Int:Int]()
var vegFeastExtrasPineapple = [Int:Int]()
var vegFeastExtrasCheese = [Int:Int]()
var vegFeastThinBases = 0

var hamAndPineappleQuantity = 0
var hamAndPineappleExtrasChillie = [Int:Int]()
var hamAndPineappleExtrasPineapple = [Int:Int]()
var hamAndPineappleExtrasCheese = [Int:Int]()
var hamAndPineappleThinBases = 0

var shandyQuantity = 0
var colaQuantity = 0
var lemonadeQuantity = 0
