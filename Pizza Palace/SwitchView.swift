//
//  ViewController.swift
//  Pizza Palace
//
//  Created by David Keen on 11/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Cocoa

class SwitchView: NSViewController, NSComboBoxDelegate {

    //Create the links to the GUI
    @IBOutlet weak var cheeseAndTomatoSummery: NSTextField!
    @IBOutlet weak var pepperoniSummery: NSTextField!
    @IBOutlet weak var hamAndPineappleSummery: NSTextField!
    @IBOutlet weak var meatFeastSummery: NSTextField!
    @IBOutlet weak var vegFeastSummery: NSTextField!
    @IBOutlet weak var lblTotal: NSComboBox!
    
    @IBOutlet weak var tableCombo: NSTextField!
    
    //Code that is run after the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        //Call the updateSummery function every second
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateSummery"), userInfo: nil, repeats: true)
        //Set the delegate of the table number combo box so we can detect when the item changed
        tableCombo.delegate = self
    }
    
    func updateSummery()
    {
        //Update the summery for each pizza + toppings
        updateCheeseAndTomatoSummery()
        updateMeetFeastSummery()
        updatePepperoniSummery()
        updateVegFeastSummery()
        updateHamAndPineappleSummery()
        
        //Set the total cost to 0 and initilise an array with the quantity of extras
        var totalCost = 0
        let cheeseAndTomatoExtra = [cheeseAndTomatoExtrasChillie,cheeseAndTomatoExtrasPineapple,cheeseAndTomatoExtrasCheese]
        let meetFeastExtra = [meetFeastExtrasChillie, meetFeastExtrasPineapple, meetFeastExtrasCheese]
        let pepperoniExtra = [pepperoniExtrasChillie, pepperoniExtrasPineapple, pepperoniExtrasCheese]
        let vegFeastExtra = [vegFeastExtrasChillie, vegFeastExtrasPineapple, vegFeastExtrasCheese]
        let hamAndPineappleExtra = [hamAndPineappleExtrasChillie, hamAndPineappleExtrasPineapple, hamAndPineappleExtrasCheese]
        let allExtras = [cheeseAndTomatoExtra,meetFeastExtra, pepperoniExtra, vegFeastExtra, hamAndPineappleExtra]
        
        //Add all the cost for the pizzas and extras
        for index in 0...2
        {
            //Get every pizza topping array
            for extraArray in allExtras
            {
                //Find the number of each topping and duplicates
                for (ammount, quantity) in extraArray[index]
                {
                    //Add 50p to the total cost
                    totalCost += ammount*(quantity*50)
                }
            }
        }
        
        //Do the same with the drinks and pizza quantity
        totalCost += shandyQuantity*120
        totalCost += colaQuantity*90
        totalCost += lemonadeQuantity*90
        totalCost += cheeseAndTomatoQuantity*795
        totalCost += pepperoniQuantity*895
        totalCost += hamAndPineappleQuantity*895
        totalCost += meetFeastQuantity*995
        totalCost += vegFeastQuantity*896
        
        /*Convert the Integer value of total cost to a float, format it to 2dp
        and display it to the total cost variable*/
        lblTotal.stringValue = (Float(totalCost)/Float(100)).format(".2")
    }
    
    
    func updateCheeseAndTomatoSummery()
    {
        //Update the summery for cheese and tomato
        cheeseAndTomatoSummery.stringValue = "Cheese and tomato"
        if cheeseAndTomatoQuantity > 0
        {
            cheeseAndTomatoSummery.stringValue.appendContentsOf(" x\(cheeseAndTomatoQuantity)")
        }
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n")
        cheeseAndTomatoSummery.stringValue.appendContentsOf("    Extra chillie\n")
        //Find the ammount of extras and how many duplicates there is for the chillies
        for (key,value) in cheeseAndTomatoExtrasChillie
        {
            if key != 0
            {
                cheeseAndTomatoSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        //Do the same for the pineapple extra
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in cheeseAndTomatoExtrasPineapple
        {
            if key != 0
            {
                cheeseAndTomatoSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        //Same for cheese
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in cheeseAndTomatoExtrasCheese
        {
            if key != 0
            {
                cheeseAndTomatoSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        //Display "Thin Bases" in the summery view
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        //If there are thin bases, display how many there are
        if cheeseAndTomatoThinBases > 0
        {
            cheeseAndTomatoSummery.stringValue.appendContentsOf("        \(cheeseAndTomatoThinBases) pizzas with thin bases")
        }
        
        
    }
    
    func updateMeetFeastSummery()
    {
        //Do the same for this pizza
        meatFeastSummery.stringValue = "Meat Feast"
        if meetFeastQuantity > 0
        {
            meatFeastSummery.stringValue.appendContentsOf(" x\(meetFeastQuantity)")
        }
        meatFeastSummery.stringValue.appendContentsOf("\n")
        print("")
        meatFeastSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in meetFeastExtrasChillie
        {
            if key != 0
            {
                meatFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        meatFeastSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in meetFeastExtrasPineapple
        {
            if key != 0
            {
                meatFeastSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        meatFeastSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in meetFeastExtrasCheese
        {
            if key != 0
            {
                meatFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        meatFeastSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if meetFeastThinBases > 0
        {
            meatFeastSummery.stringValue.appendContentsOf("        \(meetFeastThinBases) pizzas with thin bases")
        }
    }
    
    func updatePepperoniSummery()
    {
        //And this pizza
        pepperoniSummery.stringValue = "Pepperoni"
        if pepperoniQuantity > 0
        {
            pepperoniSummery.stringValue.appendContentsOf(" x\(pepperoniQuantity)")
        }
        pepperoniSummery.stringValue.appendContentsOf("\n")
        pepperoniSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in pepperoniExtrasChillie
        {
            if key != 0
            {
                pepperoniSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        pepperoniSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in pepperoniExtrasPineapple
        {
            if key != 0
            {
                pepperoniSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        pepperoniSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in pepperoniExtrasCheese
        {
            if key != 0
            {
                pepperoniSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        pepperoniSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if pepperoniThinBases > 0
        {
            pepperoniSummery.stringValue.appendContentsOf("        \(pepperoniThinBases) pizzas with thin bases")
        }
    }
    
    func updateVegFeastSummery()
    {
        //And same here
        vegFeastSummery.stringValue = "Veg Feast"
        if vegFeastQuantity > 0
        {
            vegFeastSummery.stringValue.appendContentsOf(" x\(vegFeastQuantity)")
        }
        vegFeastSummery.stringValue.appendContentsOf("\n")
        vegFeastSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in vegFeastExtrasChillie
        {
            if key != 0
            {
                vegFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        vegFeastSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in vegFeastExtrasPineapple
        {
            if key != 0
            {
                vegFeastSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        vegFeastSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in vegFeastExtrasCheese
        {
            if key != 0
            {
                vegFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        vegFeastSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if vegFeastThinBases > 0
        {
            vegFeastSummery.stringValue.appendContentsOf("        \(vegFeastThinBases) pizzas with thin bases")
        }
    }
    
    func updateHamAndPineappleSummery()
    {
        //Last one
        hamAndPineappleSummery.stringValue = "Ham and Pineapple"
        if hamAndPineappleQuantity > 0
        {
            hamAndPineappleSummery.stringValue.appendContentsOf(" x\(hamAndPineappleQuantity)")
        }
        hamAndPineappleSummery.stringValue.appendContentsOf("\n")
        hamAndPineappleSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in hamAndPineappleExtrasChillie
        {
            if key != 0
            {
                hamAndPineappleSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        hamAndPineappleSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in hamAndPineappleExtrasPineapple
        {
            if key != 0
            {
                hamAndPineappleSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        hamAndPineappleSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in hamAndPineappleExtrasCheese
        {
            if key != 0
            {
                hamAndPineappleSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        hamAndPineappleSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if hamAndPineappleThinBases > 0
        {
            hamAndPineappleSummery.stringValue.appendContentsOf("        \(hamAndPineappleThinBases) pizzas with thin bases")
        }
    }
    
    func comboBoxSelectionDidChange(comboBox:NSNotification)
    {
        //Detect if table number changed, if it did reset everything
        cheeseAndTomatoQuantity = 0
        cheeseAndTomatoExtrasChillie = [Int:Int]()
        cheeseAndTomatoExtrasPineapple = [Int:Int]()
        cheeseAndTomatoExtrasCheese = [Int:Int]()
        cheeseAndTomatoThinBases = 0
        
        meetFeastQuantity = 0
        meetFeastExtrasChillie = [Int:Int]()
        meetFeastExtrasPineapple = [Int:Int]()
        meetFeastExtrasCheese = [Int:Int]()
        meetFeastThinBases = 0
        
        pepperoniQuantity = 0
        pepperoniExtrasChillie = [Int:Int]()
        pepperoniExtrasPineapple = [Int:Int]()
        pepperoniExtrasCheese = [Int:Int]()
        pepperoniThinBases = 0
        
        vegFeastQuantity = 0
        vegFeastExtrasChillie = [Int:Int]()
        vegFeastExtrasPineapple = [Int:Int]()
        vegFeastExtrasCheese = [Int:Int]()
        vegFeastThinBases = 0
        
        hamAndPineappleQuantity = 0
        hamAndPineappleExtrasChillie = [Int:Int]()
        hamAndPineappleExtrasPineapple = [Int:Int]()
        hamAndPineappleExtrasCheese = [Int:Int]()
        hamAndPineappleThinBases = 0
        
        shandyQuantity = 0
        colaQuantity = 0
        lemonadeQuantity = 0
        
        updateSummery()
    }
}

