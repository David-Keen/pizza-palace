//
//  pizzaView.swift
//  Pizza Palace
//
//  Created by David Keen on 11/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Cocoa

class pizzaView: NSViewController {

    @IBOutlet weak var cheeseAndTomatoSummery: NSTextField!
    @IBOutlet weak var pepperoniSummery: NSTextField!
    @IBOutlet weak var hamAndPineappleSummery: NSTextField!
    @IBOutlet weak var meatFeastSummery: NSTextField!
    @IBOutlet weak var vegFeastSummery: NSTextField!
    @IBOutlet weak var lblTotal: NSTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateSummery"), userInfo: nil, repeats: true)
        // Do view setup here.
    }
    
    func updateSummery()
    {
        updateCheeseAndTomatoSummery()
        updateMeetFeastSummery()
        updatePepperoniSummery()
        updateVegFeastSummery()
        updateHamAndPineappleSummery()
        
        var totalCost = 0
        let cheeseAndTomatoExtra = [cheeseAndTomatoExtrasChillie,cheeseAndTomatoExtrasPineapple,cheeseAndTomatoExtrasCheese]
        let meetFeastExtra = [meetFeastExtrasChillie, meetFeastExtrasPineapple, meetFeastExtrasCheese]
        let pepperoniExtra = [pepperoniExtrasChillie, pepperoniExtrasPineapple, pepperoniExtrasCheese]
        let vegFeastExtra = [vegFeastExtrasChillie, vegFeastExtrasPineapple, vegFeastExtrasCheese]
        let hamAndPineappleExtra = [hamAndPineappleExtrasChillie, hamAndPineappleExtrasPineapple, hamAndPineappleExtrasCheese]
        let allExtras = [cheeseAndTomatoExtra,meetFeastExtra, pepperoniExtra, vegFeastExtra, hamAndPineappleExtra]
        
        for index in 0...2
        {
            for extraArray in allExtras
            {
                for (ammount, quantity) in extraArray[index]
                {
                    totalCost += ammount*(quantity*50)
                }
            }
        }
        
        totalCost += shandyQuantity*120
        totalCost += colaQuantity*90
        totalCost += lemonadeQuantity*90
        totalCost += cheeseAndTomatoQuantity*795
        totalCost += pepperoniQuantity*895
        totalCost += hamAndPineappleQuantity*895
        totalCost += meetFeastQuantity*995
        totalCost += vegFeastQuantity*896
        
        let floatCost:Float = Float(totalCost)/Float(100)
        lblTotal.stringValue = floatCost.format(".2")
    }
    
    
    func updateCheeseAndTomatoSummery()
    {
        cheeseAndTomatoSummery.stringValue = "Cheese and tomato"
        if cheeseAndTomatoQuantity > 0
        {
            cheeseAndTomatoSummery.stringValue.appendContentsOf(" x\(cheeseAndTomatoQuantity)")
        }
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n")
        cheeseAndTomatoSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in cheeseAndTomatoExtrasChillie
        {
            if key != 0
            {
                cheeseAndTomatoSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in cheeseAndTomatoExtrasPineapple
        {
            if key != 0
            {
                cheeseAndTomatoSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in cheeseAndTomatoExtrasCheese
        {
            if key != 0
            {
                cheeseAndTomatoSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        cheeseAndTomatoSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if cheeseAndTomatoThinBases > 0
        {
            cheeseAndTomatoSummery.stringValue.appendContentsOf("        \(cheeseAndTomatoThinBases) pizzas with thin bases")
        }
        
        
    }
    
    func updateMeetFeastSummery()
    {
        meatFeastSummery.stringValue = "Meat Feast"
        if meetFeastQuantity > 0
        {
            meatFeastSummery.stringValue.appendContentsOf(" x\(meetFeastQuantity)")
        }
        meatFeastSummery.stringValue.appendContentsOf("\n")
        print("")
        meatFeastSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in meetFeastExtrasChillie
        {
            if key != 0
            {
                meatFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        meatFeastSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in meetFeastExtrasPineapple
        {
            if key != 0
            {
                meatFeastSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        meatFeastSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in meetFeastExtrasCheese
        {
            if key != 0
            {
                meatFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        meatFeastSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if meetFeastThinBases > 0
        {
            meatFeastSummery.stringValue.appendContentsOf("        \(meetFeastThinBases) pizzas with thin bases")
        }
    }
    
    func updatePepperoniSummery()
    {
        pepperoniSummery.stringValue = "Pepperoni"
        if pepperoniQuantity > 0
        {
            pepperoniSummery.stringValue.appendContentsOf(" x\(pepperoniQuantity)")
        }
        pepperoniSummery.stringValue.appendContentsOf("\n")
        pepperoniSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in pepperoniExtrasChillie
        {
            if key != 0
            {
                pepperoniSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        pepperoniSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in pepperoniExtrasPineapple
        {
            if key != 0
            {
                pepperoniSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        pepperoniSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in pepperoniExtrasCheese
        {
            if key != 0
            {
                pepperoniSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        pepperoniSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if pepperoniThinBases > 0
        {
            pepperoniSummery.stringValue.appendContentsOf("        \(pepperoniThinBases) pizzas with thin bases")
        }
    }
    
    func updateVegFeastSummery()
    {
        vegFeastSummery.stringValue = "Veg Feast"
        if vegFeastQuantity > 0
        {
            vegFeastSummery.stringValue.appendContentsOf(" x\(vegFeastQuantity)")
        }
        vegFeastSummery.stringValue.appendContentsOf("\n")
        vegFeastSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in vegFeastExtrasChillie
        {
            if key != 0
            {
                vegFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        vegFeastSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in vegFeastExtrasPineapple
        {
            if key != 0
            {
                vegFeastSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        vegFeastSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in vegFeastExtrasCheese
        {
            if key != 0
            {
                vegFeastSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        vegFeastSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if vegFeastThinBases > 0
        {
            vegFeastSummery.stringValue.appendContentsOf("        \(vegFeastThinBases) pizzas with thin bases")
        }
    }
    
    func updateHamAndPineappleSummery()
    {
        hamAndPineappleSummery.stringValue = "Ham and Pineapple"
        if hamAndPineappleQuantity > 0
        {
            hamAndPineappleSummery.stringValue.appendContentsOf(" x\(hamAndPineappleQuantity)")
        }
        hamAndPineappleSummery.stringValue.appendContentsOf("\n")
        hamAndPineappleSummery.stringValue.appendContentsOf("    Extra chillie\n")
        for (key,value) in hamAndPineappleExtrasChillie
        {
            if key != 0
            {
                hamAndPineappleSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key) lots")
            }
        }
        
        hamAndPineappleSummery.stringValue.appendContentsOf("\n    Extra pineapple\n")
        for (key,value) in hamAndPineappleExtrasPineapple
        {
            if key != 0
            {
                hamAndPineappleSummery.stringValue.appendContentsOf(" \n       \(value) pizzas with \(key)x lots")
            }
        }
        
        hamAndPineappleSummery.stringValue.appendContentsOf("\n    Extra cheese\n")
        for (key,value) in hamAndPineappleExtrasCheese
        {
            if key != 0
            {
                hamAndPineappleSummery.stringValue.appendContentsOf("\n        \(value) pizzas with \(key)x lots")
            }
        }
        
        hamAndPineappleSummery.stringValue.appendContentsOf("\n    Thin Bases\n")
        if hamAndPineappleThinBases > 0
        {
            hamAndPineappleSummery.stringValue.appendContentsOf("        \(hamAndPineappleThinBases) pizzas with thin bases")
        }
    }
    
    @IBAction func backView(sender: AnyObject) {
        self.view.window?.close()
    }
}
