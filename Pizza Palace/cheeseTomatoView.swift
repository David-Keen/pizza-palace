//
//  cheeseTomatoView.swift
//  Pizza Palace
//
//  Created by David Keen on 11/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Cocoa

class cheeseTomatoView: NSViewController
{
    //Link all graphical objects to the code
    @IBOutlet weak var pizzaEnabled0: NSButton!
    @IBOutlet weak var pizzaEnabled1: NSButton!
    @IBOutlet weak var pizzaEnabled2: NSButton!
    @IBOutlet weak var pizzaEnabled3: NSButton!
    @IBOutlet weak var pizzaEnabled4: NSButton!
    @IBOutlet weak var pizzaEnabled5: NSButton!
    @IBOutlet weak var pizzaEnabled6: NSButton!
    @IBOutlet weak var pizzaEnabled7: NSButton!
    @IBOutlet weak var pizzaEnabled8: NSButton!
    @IBOutlet weak var pizzaEnabled9: NSButton!
    
    @IBOutlet weak var thinBased0: NSButton!
    @IBOutlet weak var thinBased1: NSButton!
    @IBOutlet weak var thinBased2: NSButton!
    @IBOutlet weak var thinBased3: NSButton!
    @IBOutlet weak var thinBased4: NSButton!
    @IBOutlet weak var thinBased5: NSButton!
    @IBOutlet weak var thinBased6: NSButton!
    @IBOutlet weak var thinBased7: NSButton!
    @IBOutlet weak var thinBased8: NSButton!
    @IBOutlet weak var thinBased9: NSButton!
    
    @IBOutlet weak var chillieAmmount0: NSTextField!
    @IBOutlet weak var chillieAmmount1: NSTextField!
    @IBOutlet weak var chillieAmmount2: NSTextField!
    @IBOutlet weak var chillieAmmount3: NSTextField!
    @IBOutlet weak var chillieAmmount4: NSTextField!
    @IBOutlet weak var chillieAmmount5: NSTextField!
    @IBOutlet weak var chillieAmmount6: NSTextField!
    @IBOutlet weak var chillieAmmount7: NSTextField!
    @IBOutlet weak var chillieAmmount8: NSTextField!
    @IBOutlet weak var chillieAmmount9: NSTextField!
    
    @IBOutlet weak var pineappleAmmount0: NSTextField!
    @IBOutlet weak var pineappleAmmount1: NSTextField!
    @IBOutlet weak var pineappleAmmount2: NSTextField!
    @IBOutlet weak var pineappleAmmount3: NSTextField!
    @IBOutlet weak var pineappleAmmount4: NSTextField!
    @IBOutlet weak var pineappleAmmount5: NSTextField!
    @IBOutlet weak var pineappleAmmount6: NSTextField!
    @IBOutlet weak var pineappleAmmount7: NSTextField!
    @IBOutlet weak var pineappleAmmount8: NSTextField!
    @IBOutlet weak var pineappleAmmount9: NSTextField!
    
    @IBOutlet weak var cheeseAmmount0: NSTextField!
    @IBOutlet weak var cheeseAmmount1: NSTextField!
    @IBOutlet weak var cheeseAmmount2: NSTextField!
    @IBOutlet weak var cheeseAmmount3: NSTextField!
    @IBOutlet weak var cheeseAmmount4: NSTextField!
    @IBOutlet weak var cheeseAmmount5: NSTextField!
    @IBOutlet weak var cheeseAmmount6: NSTextField!
    @IBOutlet weak var cheeseAmmount7: NSTextField!
    @IBOutlet weak var cheeseAmmount8: NSTextField!
    @IBOutlet weak var cheeseAmmount9: NSTextField!
    
    @IBOutlet weak var requests0: NSTextField!
    @IBOutlet weak var requests1: NSTextField!
    @IBOutlet weak var requests2: NSTextField!
    @IBOutlet weak var requests3: NSTextField!
    @IBOutlet weak var requests4: NSTextField!
    @IBOutlet weak var requests5: NSTextField!
    @IBOutlet weak var requests6: NSTextField!
    @IBOutlet weak var requests7: NSTextField!
    @IBOutlet weak var requests8: NSTextField!
    @IBOutlet weak var requests9: NSTextField!
    
    //Reset extras, quantity and the thin bases to nothing
    override func viewDidLoad() {
        cheeseAndTomatoQuantity = 0
        cheeseAndTomatoExtrasChillie = [Int:Int]()
        cheeseAndTomatoExtrasPineapple = [Int:Int]()
        cheeseAndTomatoExtrasCheese = [Int:Int]()
        cheeseAndTomatoThinBases = 0
        super.viewDidLoad()
    }
    
    
    //Called when you add/enable a pizza
    @IBAction func pizzaPressed(sender: AnyObject)
    {
        //Convert AnyObject to NSButton so we know the methods and variables
        let object = sender as! NSButton
        //Get the state of the checkbox (enabled(1) or disabled(0))
        let state = object.state
        //Get the ID of the obkect
        let tag = object.tag
        print("The state of check box \(tag) is \(state)")
        
        //Logic of case tag
        //If ID is x = (row number + 1)
          //If the user disabled it
            //Disable all rows/pizzas after it
          //Else enable that row and the button below
        
        switch tag
        {
        case 0:
            if pizzaEnabled0.state == 0
            {
                deleteRowXfromBottom(9)
            } else{
                enableRow(1)
            }
        case 1:
            if pizzaEnabled1.state == 0
            {
                deleteRowXfromBottom(8)
            } else {
                enableRow(2)
            }
        case 2:
            if pizzaEnabled2.state == 0
            {
                deleteRowXfromBottom(7)
            } else {
                enableRow(3)
            }
        case 3:
            if pizzaEnabled3.state == 0
            {
                deleteRowXfromBottom(6)
            } else {
                enableRow(4)
            }
        case 4:
            if pizzaEnabled4.state == 0
            {
                deleteRowXfromBottom(5)
            } else {
                enableRow(5)
            }
        case 5:
            if pizzaEnabled5.state == 0
            {
                pizzaEnabled6.enabled = true
            } else {
                enableRow(6)
            }
        case 6:
            if pizzaEnabled6.state == 0
            {
                deleteRowXfromBottom(3)
            } else {
                enableRow(7)
            }
        case 7:
            if pizzaEnabled7.state == 0
            {
                deleteRowXfromBottom(2)
            } else {
                enableRow(8)
            }
        case 8:
            if pizzaEnabled8.state == 0
            {
                deleteRowXfromBottom(1)
            } else {
                enableRow(9)
            }
        case 9:
            if state == 1
            {
                //Enable the current row if allowed to
                thinBased9.enabled = true
                chillieAmmount9.enabled = true
                pineappleAmmount9.enabled = true
                cheeseAmmount9.enabled = true
                requests9.enabled = true
            } else {
                thinBased9.enabled = false
                chillieAmmount9.enabled = false
                pineappleAmmount9.enabled = false
                cheeseAmmount9.enabled = false
                requests9.enabled = false
            }
            //Somthing didnt get checked
        default :
            print("Unknown pizza box enabled")
        }
    }
    
    //Disable x ammount of rows/pizzas from the bottom
    func deleteRowXfromBottom(toRow:Int)
    {
        let pizzaArray = [pizzaEnabled9,pizzaEnabled8,pizzaEnabled7,pizzaEnabled6,pizzaEnabled5,pizzaEnabled4,pizzaEnabled3,pizzaEnabled2,pizzaEnabled1,pizzaEnabled0]
        let deleteUntill = toRow-1
        //Same as for(int index = 0; index<=deleteUntill; index++) in C++
        for index in 0...deleteUntill
        {
            pizzaArray[index].state = 0
            pizzaArray[index].enabled = false
        }
    }
    
    //Same thing but enabling the current row instead
    func enableRow(rowToEnable:Int)
    {
        let enabledRow = rowToEnable-1
        let pizzaEnabler = [pizzaEnabled1,pizzaEnabled2,pizzaEnabled3,pizzaEnabled4,pizzaEnabled5,pizzaEnabled6,pizzaEnabled7,pizzaEnabled8,pizzaEnabled9]
        let thinBaseEnabler = [thinBased0,thinBased1,thinBased2,thinBased3,thinBased4,thinBased5,thinBased6,thinBased7,thinBased8]
        
        let chilliesEnabler = [chillieAmmount0,chillieAmmount1,chillieAmmount2,chillieAmmount3,chillieAmmount4,chillieAmmount5,chillieAmmount6,chillieAmmount7,chillieAmmount8]
        let pineapplRnabler = [pineappleAmmount0,pineappleAmmount1,pineappleAmmount2,pineappleAmmount3,pineappleAmmount4,pineappleAmmount5,pineappleAmmount6,pineappleAmmount7,pineappleAmmount8,pineappleAmmount9]
        let cheeseEnabler = [cheeseAmmount0,cheeseAmmount1,cheeseAmmount2,cheeseAmmount3,cheeseAmmount4,cheeseAmmount5,cheeseAmmount6,cheeseAmmount7,cheeseAmmount8]
        let requestEnabler = [requests0,requests1,requests2,requests3,requests4,requests5,requests6,requests7,requests8]
        
        pizzaEnabler[enabledRow].enabled = true
        thinBaseEnabler[enabledRow].enabled = true
        chilliesEnabler[enabledRow].enabled = true
        pineapplRnabler[enabledRow].enabled = true
        cheeseEnabler[enabledRow].enabled = true
        requestEnabler[enabledRow].enabled = true
    }
    
    //Calculate the quantity of everything and dismiss the view
    @IBAction func done(sender:AnyObject)
    {
        let pizzas = [pizzaEnabled0, pizzaEnabled1,pizzaEnabled2,pizzaEnabled3,pizzaEnabled4,pizzaEnabled5,pizzaEnabled6,pizzaEnabled7,pizzaEnabled8,pizzaEnabled9]
        let bases = [thinBased0,thinBased1,thinBased2,thinBased3,thinBased4,thinBased5,thinBased6,thinBased7,thinBased8,thinBased9]
        
        let chillieAmmount = [chillieAmmount0,chillieAmmount1,chillieAmmount2,chillieAmmount3,chillieAmmount4,chillieAmmount5,chillieAmmount6,chillieAmmount7,chillieAmmount8, chillieAmmount9]
        
        let pineappeAmmount = [pineappleAmmount0,pineappleAmmount1,pineappleAmmount2,pineappleAmmount3,pineappleAmmount4,pineappleAmmount5,pineappleAmmount6,pineappleAmmount7,pineappleAmmount8,pineappleAmmount9]
        
        let cheeseAmmount = [cheeseAmmount0,cheeseAmmount1,cheeseAmmount2,cheeseAmmount3,cheeseAmmount4,cheeseAmmount5,cheeseAmmount6,cheeseAmmount7,cheeseAmmount8,cheeseAmmount9]
        
        let requests = [requests0,requests1,requests2,requests3,requests4,requests5,requests6,requests7,requests8,requests9] //Will be used in a later version to display customer requests
        
        self.view.window?.close() //Here we dismiss the view
                for extra in chillieAmmount
        {
            if let _ = cheeseAndTomatoExtrasChillie[extra.integerValue]
            {
                cheeseAndTomatoExtrasChillie[extra.integerValue] = cheeseAndTomatoExtrasChillie[extra.integerValue]! + 1
            } else {
                cheeseAndTomatoExtrasChillie[extra.integerValue] = 1
            }
        }
        
        for extra in pineappeAmmount
        {
            if let _ = cheeseAndTomatoExtrasPineapple[extra.integerValue]
            {
                cheeseAndTomatoExtrasPineapple[extra.integerValue] = cheeseAndTomatoExtrasPineapple[extra.integerValue]! + 1
            } else {
                cheeseAndTomatoExtrasPineapple[extra.integerValue] = 1
            }
        }
        
        for extra in cheeseAmmount
        {
            if let _ = cheeseAndTomatoExtrasCheese[extra.integerValue]
            {
                cheeseAndTomatoExtrasCheese[extra.integerValue] = cheeseAndTomatoExtrasCheese[extra.integerValue]! + 1
            } else {
                cheeseAndTomatoExtrasCheese[extra.integerValue] = 1
            }
        }
        
        for index in 0...9
        {
            print("Hello")
            if pizzas[index].state == 1
            {
                cheeseAndTomatoQuantity++
            }
            
            if bases[index].state == 1
            {
                cheeseAndTomatoThinBases++
            }
        }
        
    }
    
    
    
}
