//
//  pepperoniView.swift
//  Pizza Palace
//
//  Created by David Keen on 13/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Cocoa

class pepperoniView: NSViewController {

    @IBOutlet weak var pizzaEnabled0: NSButton!
    @IBOutlet weak var pizzaEnabled1: NSButton!
    @IBOutlet weak var pizzaEnabled2: NSButton!
    @IBOutlet weak var pizzaEnabled3: NSButton!
    @IBOutlet weak var pizzaEnabled4: NSButton!
    @IBOutlet weak var pizzaEnabled5: NSButton!
    @IBOutlet weak var pizzaEnabled6: NSButton!
    @IBOutlet weak var pizzaEnabled7: NSButton!
    @IBOutlet weak var pizzaEnabled8: NSButton!
    @IBOutlet weak var pizzaEnabled9: NSButton!
    
    @IBOutlet weak var thinBased0: NSButton!
    @IBOutlet weak var thinBased1: NSButton!
    @IBOutlet weak var thinBased2: NSButton!
    @IBOutlet weak var thinBased3: NSButton!
    @IBOutlet weak var thinBased4: NSButton!
    @IBOutlet weak var thinBased5: NSButton!
    @IBOutlet weak var thinBased6: NSButton!
    @IBOutlet weak var thinBased7: NSButton!
    @IBOutlet weak var thinBased8: NSButton!
    @IBOutlet weak var thinBased9: NSButton!
    
    @IBOutlet weak var chillieAmmount0: NSTextField!
    @IBOutlet weak var chillieAmmount1: NSTextField!
    @IBOutlet weak var chillieAmmount2: NSTextField!
    @IBOutlet weak var chillieAmmount3: NSTextField!
    @IBOutlet weak var chillieAmmount4: NSTextField!
    @IBOutlet weak var chillieAmmount5: NSTextField!
    @IBOutlet weak var chillieAmmount6: NSTextField!
    @IBOutlet weak var chillieAmmount7: NSTextField!
    @IBOutlet weak var chillieAmmount8: NSTextField!
    @IBOutlet weak var chillieAmmount9: NSTextField!
    
    @IBOutlet weak var pineappleAmmount0: NSTextField!
    @IBOutlet weak var pineappleAmmount1: NSTextField!
    @IBOutlet weak var pineappleAmmount2: NSTextField!
    @IBOutlet weak var pineappleAmmount3: NSTextField!
    @IBOutlet weak var pineappleAmmount4: NSTextField!
    @IBOutlet weak var pineappleAmmount5: NSTextField!
    @IBOutlet weak var pineappleAmmount6: NSTextField!
    @IBOutlet weak var pineappleAmmount7: NSTextField!
    @IBOutlet weak var pineappleAmmount8: NSTextField!
    @IBOutlet weak var pineappleAmmount9: NSTextField!
    
    @IBOutlet weak var cheeseAmmount0: NSTextField!
    @IBOutlet weak var cheeseAmmount1: NSTextField!
    @IBOutlet weak var cheeseAmmount2: NSTextField!
    @IBOutlet weak var cheeseAmmount3: NSTextField!
    @IBOutlet weak var cheeseAmmount4: NSTextField!
    @IBOutlet weak var cheeseAmmount5: NSTextField!
    @IBOutlet weak var cheeseAmmount6: NSTextField!
    @IBOutlet weak var cheeseAmmount7: NSTextField!
    @IBOutlet weak var cheeseAmmount8: NSTextField!
    @IBOutlet weak var cheeseAmmount9: NSTextField!
    
    @IBOutlet weak var requests0: NSTextField!
    @IBOutlet weak var requests1: NSTextField!
    @IBOutlet weak var requests2: NSTextField!
    @IBOutlet weak var requests3: NSTextField!
    @IBOutlet weak var requests4: NSTextField!
    @IBOutlet weak var requests5: NSTextField!
    @IBOutlet weak var requests6: NSTextField!
    @IBOutlet weak var requests7: NSTextField!
    @IBOutlet weak var requests8: NSTextField!
    @IBOutlet weak var requests9: NSTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pepperoniQuantity = 0
        pepperoniExtrasChillie = [Int:Int]()
        pepperoniExtrasPineapple = [Int:Int]()
        pepperoniExtrasCheese = [Int:Int]()
    }
    
    @IBAction func backView(sender: AnyObject) {
        self.view.window?.close()
    }
    
    
    @IBAction func pizzaPressed(sender: AnyObject)
    {
        let object = sender as! NSButton
        let state = object.state
        let tag = object.tag
        print("The state of check box \(tag) is \(state)")
        
        switch tag
        {
        case 0:
            print("0 enabled")
            if pizzaEnabled0.state == 0
            {
                deleteRowXfromBottom(9)
            } else{
                enableRow(1)
            }
        case 1:
            if pizzaEnabled1.state == 0
            {
                deleteRowXfromBottom(8)
            } else {
                enableRow(2)
            }
        case 2:
            if pizzaEnabled2.state == 0
            {
                deleteRowXfromBottom(7)
            } else {
                enableRow(3)
            }
        case 3:
            if pizzaEnabled3.state == 0
            {
                deleteRowXfromBottom(6)
            } else {
                enableRow(4)
            }
        case 4:
            if pizzaEnabled4.state == 0
            {
                deleteRowXfromBottom(5)
            } else {
                enableRow(5)
            }
        case 5:
            if pizzaEnabled5.state == 0
            {
                pizzaEnabled6.enabled = true
            } else {
                enableRow(6)
            }
        case 6:
            if pizzaEnabled6.state == 0
            {
                deleteRowXfromBottom(3)
            } else {
                enableRow(7)
            }
        case 7:
            if pizzaEnabled7.state == 0
            {
                deleteRowXfromBottom(2)
            } else {
                enableRow(8)
            }
        case 8:
            if pizzaEnabled8.state == 0
            {
                deleteRowXfromBottom(1)
            } else {
                enableRow(9)
            }
        case 9:
            if state == 1
            {
                thinBased9.enabled = true
                chillieAmmount9.enabled = true
                pineappleAmmount9.enabled = true
                cheeseAmmount9.enabled = true
                requests9.enabled = true
            }
        default :
            print("Unknown pizza box enabled")
        }
    }
    
    
    func deleteRowXfromBottom(toRow:Int)
    {
        let pizzaArray = [pizzaEnabled9,pizzaEnabled8,pizzaEnabled7,pizzaEnabled6,pizzaEnabled5,pizzaEnabled4,pizzaEnabled3,pizzaEnabled2,pizzaEnabled1,pizzaEnabled0]
        let deleteUntill = toRow-1
        for index in 0...deleteUntill
        {
            pizzaArray[index].state = 0
            pizzaArray[index].enabled = false
        }
    }
    
    func enableRow(rowToEnable:Int)
    {
        let enabledRow = rowToEnable-1
        let pizzaEnabler = [pizzaEnabled1,pizzaEnabled2,pizzaEnabled3,pizzaEnabled4,pizzaEnabled5,pizzaEnabled6,pizzaEnabled7,pizzaEnabled8,pizzaEnabled9]
        let thinBaseEnabler = [thinBased0,thinBased1,thinBased2,thinBased3,thinBased4,thinBased5,thinBased6,thinBased7,thinBased8]
        
        let chilliesEnabler = [chillieAmmount0,chillieAmmount1,chillieAmmount2,chillieAmmount3,chillieAmmount4,chillieAmmount5,chillieAmmount6,chillieAmmount7,chillieAmmount8]
        let pineapplRnabler = [pineappleAmmount0,pineappleAmmount1,pineappleAmmount2,pineappleAmmount3,pineappleAmmount4,pineappleAmmount5,pineappleAmmount6,pineappleAmmount7,pineappleAmmount8,pineappleAmmount9]
        let cheeseEnabler = [cheeseAmmount0,cheeseAmmount1,cheeseAmmount2,cheeseAmmount3,cheeseAmmount4,cheeseAmmount5,cheeseAmmount6,cheeseAmmount7,cheeseAmmount8]
        let requestEnabler = [requests0,requests1,requests2,requests3,requests4,requests5,requests6,requests7,requests8]
        
        pizzaEnabler[enabledRow].enabled = true
        thinBaseEnabler[enabledRow].enabled = true
        chilliesEnabler[enabledRow].enabled = true
        pineapplRnabler[enabledRow].enabled = true
        cheeseEnabler[enabledRow].enabled = true
        requestEnabler[enabledRow].enabled = true
    }
    
    @IBAction func done(sender:AnyObject)
    {
        let pizzas = [pizzaEnabled0, pizzaEnabled1,pizzaEnabled2,pizzaEnabled3,pizzaEnabled4,pizzaEnabled5,pizzaEnabled6,pizzaEnabled7,pizzaEnabled8,pizzaEnabled9]
        let bases = [thinBased0,thinBased1,thinBased2,thinBased3,thinBased4,thinBased5,thinBased6,thinBased7,thinBased8,thinBased9]
        
        let chillieAmmount = [chillieAmmount0,chillieAmmount1,chillieAmmount2,chillieAmmount3,chillieAmmount4,chillieAmmount5,chillieAmmount6,chillieAmmount7,chillieAmmount8, chillieAmmount9]
        
        let pineappeAmmount = [pineappleAmmount0,pineappleAmmount1,pineappleAmmount2,pineappleAmmount3,pineappleAmmount4,pineappleAmmount5,pineappleAmmount6,pineappleAmmount7,pineappleAmmount8,pineappleAmmount9]
        
        let cheeseAmmount = [cheeseAmmount0,cheeseAmmount1,cheeseAmmount2,cheeseAmmount3,cheeseAmmount4,cheeseAmmount5,cheeseAmmount6,cheeseAmmount7,cheeseAmmount8,cheeseAmmount9]
        
        let requests = [requests0,requests1,requests2,requests3,requests4,requests5,requests6,requests7,requests8,requests9]
        
        self.view.window?.close()
        for extra in chillieAmmount
        {
            print("Its cold")
            if let _ = pepperoniExtrasChillie[extra.integerValue]
            {
                pepperoniExtrasChillie[extra.integerValue] = pepperoniExtrasChillie[extra.integerValue]! + 1
            } else {
                pepperoniExtrasChillie[extra.integerValue] = 1
            }
        }
        
        for extra in pineappeAmmount
        {
            if let _ = pepperoniExtrasPineapple[extra.integerValue]
            {
                pepperoniExtrasPineapple[extra.integerValue] = pepperoniExtrasPineapple[extra.integerValue]! + 1
            } else {
                pepperoniExtrasPineapple[extra.integerValue] = 1
            }
        }
        
        for extra in cheeseAmmount
        {
            if let _ = pepperoniExtrasCheese[extra.integerValue]
            {
                pepperoniExtrasCheese[extra.integerValue] = pepperoniExtrasCheese[extra.integerValue]! + 1
            } else {
                pepperoniExtrasCheese[extra.integerValue] = 1
            }
        }
        
        for index in 0...9
        {
            print("Hello")
            if pizzas[index].state == 1
            {
                pepperoniQuantity++
            }
            
            if bases[index].state == 1
            {
                pepperoniThinBases++
            }
        }
        
        
        
    }
    
}
