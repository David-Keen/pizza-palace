//
//  extensions.swift
//  Pizza Palace
//
//  Created by David Keen on 13/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Foundation

//Format a Float value to 2dp
extension Float {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f", self) as String
    }
}