//
//  drinksView.swift
//  Pizza Palace
//
//  Created by David Keen on 11/01/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Cocoa

class drinksView: NSViewController {

    //Link to the Graphical interface text fields
    @IBOutlet weak var shandyAmmount: NSTextField!
    @IBOutlet weak var colaAmmount: NSTextField!
    @IBOutlet weak var lemonadeAmmount: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Set the global values to the ammount of each drink type
    @IBAction func backView(sender: AnyObject) {
        shandyQuantity = shandyAmmount.integerValue
        colaQuantity = colaAmmount.integerValue
        lemonadeQuantity = lemonadeAmmount.integerValue
        print("There are\n\(shandyQuantity)x shandy\n\(colaQuantity)x Cola\n\(lemonadeQuantity)x lemonade")
        self.view.window?.close() //This closes the window
    }
    
}
